#!/bin/bash

softPath="$(pwd)"

gpg configuration.tar.gz.gpg
tar xzf configuration.tar.gz

rsync -a  configuration/ ~/
rm configuration -r
rm configuration.tar.gz

