#!/bin/bash

softPath="$(pwd)"

# ssh-keygen -t ed25519

mkdir configuration

cp ~/.ssh configuration -r
cp ~/.config configuration -r
cp ~/snap configuration -r
cp ~/.local configuration -r
tar czf configuration.tar.gz configuration
gpg -c configuration.tar.gz

rm configuration.tar.gz
rm configuration -r
